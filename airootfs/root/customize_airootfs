#!/bin/bash
# Configure live iso

set -e -u -x
shopt -s extglob

# Set locales
sed -i 's/#\(en_GB\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

# Sudo to allow no password
sed -i 's/# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/g' /etc/sudoers
chown -c root:root /etc/sudoers
chmod -c 0440 /etc/sudoers

# Hostname
echo "archbang" > /etc/hostname

#  Hosts file to archbang (test version)
sed -i "0,/127.0.0.1/s/localhost/archbang/2" /etc/hosts

# Vconsole
echo "KEYMAP=gb" > /etc/vconsole.conf
echo "FONT=" >> /etc/vconsole.conf

# Locale
echo "LANG=en_GB.UTF-8" > /etc/locale.conf
echo "LC_COLLATE=C" >> /etc/locale.conf

# Set clock to UTC
hwclock --systohc --utc

# Timezone
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime

# Logs files live
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

# Add live user
useradd -m -p "" -g users -G "wheel" -s /bin/bash ablive
chown ablive /home/ablive

# remove /etc/skel from iso build
rm -r /etc/skel/
mkdir -p /etc/skel

# path to desktop files
path="/usr/share/applications"

# desktop files to remove from openbox menu
desk_file='volumeicon gparted conky tint2conf tint2 pcmanfm-desktop-pref qv4l2 qvidcap'

for un in ${desk_file}
do
  mv ${path}/${un}.desktop ${path}/${un}.hide
done

# Fix htop looking for xterm
ln -s /usr/bin/lxterminal /usr/bin/xterm

# Start required systemd services
systemctl enable {haveged,pacman-init,NetworkManager}.service -f

#systemctl set-default multi-user.target
systemctl set-default graphical.target

